---
title: How to Set Up KVM on a Remote Server
---

[KVM][1] (for Kernel-based Virtual Machine) is a simple way to run virtual machines on a server. Virtual machines are great because they let you keep different projects separated, but still run on the same server, saving you hardware costs. Installing KVM on a remote server is pretty easy, but there aren't that many guides for it. In this article, I'll show you how to install KVM on a remote server, then create a new virtual machine on it that you can access as if it's a separate server. 

*Note: I've tested this guide on Ubuntu Server 16.04 and 18.04. It should work on other Debian/Ubuntu distributions, but I haven't tested it on those.*

## Installing KVM on the Host Server

Once you've SSHed into the host server, the first thing to do is make sure that the server's CPU supports virtualization by running this command in the terminal:

```bash
$ egrep -c '(vmx|svm)' /proc/cpuinfo
```

If it prints out `0`, then you won't be able to run KVM on the server. If the number is >= `1`, then the CPU supports virtualization but you may still need to enable it in the BIOS. 

Next, install the necessary packages (this will take a while, and when it finishes you'll need to reboot the server):

```bash
$ sudo apt-get install qemu-kvm libvirt-bin ubuntu-vm-builder bridge-utils
```

Once the server has restarted, check to make sure the installation worked by using the `virsh` command: 

```text
$ virsh list --all
 Id    Name                           State
----------------------------------------------------

$
```

Now, before we can create a virtual machine, we need an ISO file. I recommend Ubuntu Server, but you can install any OS you want in your virtual machines:

```bash
$ cd /var/lib/libvirt/images/
$ wget http://releases.ubuntu.com/16.04.3/ubuntu-16.04.3-server-amd64.iso
```

## Creating Virtual Machines

Now that KVM is set up on the server, you need to create a virtual machine. Although this can be done from the terminal on the remote server, it's much easier if you use a GUI. Thankfully, there's a perfect tool for this called `virt-manager`. Install it on your own computer, not the server:

```bash
$ sudo apt install virt-manager
```

Open it up by running `virt-manager` from the terminal, or looking for "Virtual Machine Manager" in the applications menu. You'll need to create a new connection by going to File > Add Connection in the menu. Once there, you'll need to connect to a remote host. The default method of SSH is fine, just fill in your username and the IP address of the server (or hostname if you've configured your `/etc/hosts` file properly). 

Once it's successfully connected, you can create a new virtual machine by going to File > New Virtual Machine. Proceed through the steps: 

1. Use the default option of `Local install media (ISO image or CDROM)`.  
2. Click the "Browse" button and choose the ISO file we downloaded earlier, then select the correct OS and version
3. Choose how much RAM and the number of CPU cores you want assigned to the VM (if possible, I recommend a minimum of 2GB RAM and 2 CPUs)
4.  Give the VM an appropriate amount of storage space (for small projects, the default amount is usually fine). 
5. Set the name to whatever you want, then, under `Network selection`, choose `Host device [name]: macvtap`, and make sure the source mode is set to Bridge. 

Now proceed through the OS installation like you normally would. Once it's finished, you'll be able to run commands through the virt-manager window, but you won't be able to connect using normal methods (SSH). To do that, you'll need to give the VM a serial console so it accepts connections. Using your virt-manager window, run these commands in the guest: 

```bash
                                 # ↓ capital s, not number 5
$ systemctl enable serial-getty@ttyS0.service
$ systemctl start serial-getty@ttyS0.service
```

You'll now be able to connect to the VM using SSH or the `virsh` command on the host, like this:

```bash
$ virsh console my_vm_name
```

## *Update April 14, 2020* 

The default `macvtap` interface doesn't allow host-guest communication through the network. The solution is to create a `macvlan` interface for the host to use by default. That can be done using these commands (assuming your physical interface is called `eno1`, your default gateway is at 192.168.1.1, and you want your KVM host to have the ip address 192.168.1.100):

```bash
$ ip link add link eno1 macvlan0 type macvlan mode bridge
$ ip addr show dev macvlan0
XX: macvlan0@eno1: <BROADCAST,MULTICAST> [...]
    link/ether <MAC address> brd ff:ff:ff:ff:ff:ff
$ ip link add link eno1 address <MAC> macvlan0 type macvlan mode bridge
$ ip address add 192.168.1.100/24 dev macvlan0
$ ip link set dev macvlan0 up
$ ip route flush dev eno1
$ ip route add default via 192.168.1.1 dev macvlan0 proto static
```

Sources: 

- https://superuser.com/a/368023
- https://superuser.com/a/1128513


## Conclusion

That's all it takes to set up KVM on Ubuntu! You can now quickly and easily set up as many VMs as your server can run. 

### References

- <https://help.ubuntu.com/community/KVM/Installation>
- <https://help.ubuntu.com/community/KVM/Access>

[1]: https://www.linux-kvm.org/page/Main_Page