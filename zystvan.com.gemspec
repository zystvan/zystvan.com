# coding: utf-8

Gem::Specification.new do |spec|
  spec.name          = "zystvan.com"
  spec.version       = "0.1.0"
  spec.authors       = ["Zeke Y"]
  spec.email         = ["zeke@zystvan.com"]

  spec.summary       = "Zeke Y's website"
  spec.homepage      = "https://gitlab.com/zystvan/zystvan.com-v3"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r{^(assets|_layouts|_includes|_sass|LICENSE|README)}i) }

  spec.add_runtime_dependency "jekyll", "~> 4.0"

  spec.add_development_dependency "bundler", "~> 2.0.2"
  spec.add_development_dependency "rake", "~> 13.0"
end
