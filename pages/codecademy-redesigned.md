---
layout: page
title: Codecademy Redesigned has been Retired
permalink: /codecademy-redesigned/
---

We have recently been working on bringing our improvements from Codecademy Redesigned directly into the Codecademy Discourse instance. This allows more people to get the same improvements, without anyone having to install a browser extension. If you'd like to use the addon anyway, you may install it here:

- [Firefox](https://addons.mozilla.org/en-US/firefox/addon/codecademy-redesigned)
- [Chrome](https://chrome.google.com/webstore/detail/codecademy-redesigned/bladgjamjaiaffkojoadgeelkgfgkdkp)

Since this addon is no longer being maintained, bear in mind that it may no longer be compatible with your browser version.

You can see the code on GitHub here:

- [Firefox repository](https://gitlab.com/zystvan/codecademy-redesigned-firefox)
- [Chrome repository](https://github.com/A-J-C/Codecademy-Redesign)

Many thanks to all those involved with the project for their efforts.
